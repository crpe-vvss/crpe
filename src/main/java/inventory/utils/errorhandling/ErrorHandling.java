package inventory.utils.errorhandling;

import javafx.scene.control.Alert;
import javafx.stage.Modality;

public class ErrorHandling {
    public static void displayError(String message, String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle(title);
        alert.setHeaderText("Error");
        alert.setContentText(message);
        alert.show();
    }
}
